﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectChangePosition : MonoBehaviour
{
    public float movespeed = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        movespeed++;        
        transform.position = new Vector2(transform.position.x + movespeed * Time.deltaTime, transform.position.y);
        transform.Rotate(10, 0, 0, Space.Self);
    }
}